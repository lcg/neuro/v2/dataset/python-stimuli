# lcg/v2/dataset/python-stimuli

[![][badge-version]][repository-latest-release]
[![][binder-badge]][binder-launch-latest]

[![][badge-master]]()
[![][binder-badge]][binder-launch-master]

Interactive representations of stimuli sequences presented to V2 subjects.

- For a live demonstration, click one of the ![][binder-badge] buttons above.
- For the history of changes, see the [CHANGELOG](./CHANGELOG.md).
- For a detailed description of repository contents, see the [MANIFEST](./MANIFEST.md).

## Development

1. Install [Poetry] (optional), [pre-commit] (optional), and [DVC] (already a dev-dependency)
1. Create a Python 3 virtualenv and activate it &mdash; *e.g.* `python3 -m venv .venv && source .venv/bin/activate`
1. Install dependencies &mdash; `poetry install` or `pip install -r requirements.txt` (in the virtualenv)
1. Set up [pre-commit] &mdash; `pre-commit install`
1. Reproduce [DVC] pipelines &mdash; `poetry run dvc repro`
1. Launch a [Jupyter] notebook server to explore the files &mdash; `poetry run jupyter notebook` or `jupyter notebook`

For the lazy:

```bash
python3 -m venv .venv && source .venv/bin/activate
poetry install
pre-commit install
poetry run dvc repro
poetry run jupyter notebook
```

**Notes:** [DVC] is included as a dev-dependency. If your choose not to install dev-dependencies by passing `--no-dev` to `poetry install`, for any reason, remember to install [DVC] separately.

[DVC]: https://dvc.org
[Jupyter]: https://jupyter.org
[Poetry]: https://poetry.eustace.io/
[badge-master]: https://img.shields.io/badge/version-master-orange.svg
[badge-version]: https://img.shields.io/badge/version-0.1.0-yellow.svg
[binder-badge]: http://mybinder.org/badge_logo.svg
[binder-launch-latest]: https://mybinder.org/v2/gl/lcg%2Fneuro%2Fv2%2Fdataset%2Fpython-stimuli/0.1.0
[binder-launch-master]: https://mybinder.org/v2/gl/lcg%2Fneuro%2Fv2%2Fdataset%2Fpython-stimuli/master
[pre-commit]: https://pre-commit.com/
[repository-latest-release]: https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/0.1.0
