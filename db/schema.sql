CREATE TABLE DriftingGrating (
    condition INTEGER PRIMARY KEY,
    contrast FLOAT,
    spatial_freq_deg FLOAT,
    temporal_freq_hz FLOAT,
    orientation_deg FLOAT,
    color_r FLOAT,
    color_g FLOAT,
    color_b FLOAT,
    description TEXT,

    CHECK (0 <= contrast AND contrast <= 1),
    CHECK (spatial_freq_deg > 0),
    CHECK (temporal_freq_hz > 0),
    CHECK (orientation_deg IN (0, 45, 90, 135, 180, 225, 270)),
    CHECK (0 <= color_r AND color_r <= 1),
    CHECK (0 <= color_g AND color_g <= 1),
    CHECK (0 <= color_b AND color_b <= 1)
--     UNIQUE (contrast, spatial_freq_deg, temporal_freq_hz, orientation_deg, color_r, color_g, color_b)
);