#!/bin/bash

DB_FILE=db.sqlite
SCHEMA_FILE=db/schema.sql
INPUT_FILES=$(find db -name "*.csv")

function import-csv {
    echo -e ".mode csv\n.import $1 $2" | sqlite3 ${DB_FILE}
}

# Clean database.
if [[ -s ${DB_FILE} ]]
then
    rm ${DB_FILE}
fi

# Create schema.
cat ${SCHEMA_FILE} | sqlite3 ${DB_FILE}

# Input table data from CSV files.
import-csv "db/drifting-grating.csv" "DriftingGrating"
