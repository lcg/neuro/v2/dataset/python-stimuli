# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog], and this project adheres to [Semantic Versioning].

## [Unreleased]

## [0.1.0] - 2019-07-02
### Added

* [`.dvc/`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/.dvc) &mdash; configuration files for [DVC]
    * [`.config`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/.dvc/.config)
    * [`.gitignore`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/.dvc/.gitignore)
* [`.bumpversion.cfg`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/.bumpversion.cfg) &mdash; [bumpversion] configuration.
* [`.gitignore`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/.gitignore)
* [`.pre-commit-config.yaml`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/.pre-commit-config.yaml) &mdash; [pre-commit] configuration.
* [`CHANGELOG.md`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/CHANGELOG.md) &mdash; history of changes.
* [`LICENSE.txt`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/LICENSE.txt) &mdash; copy of [MIT License].
* [`README.md`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/README.md) &mdash; repository instructions.
* [`Dvcfile`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/Dvcfile) &mdash; basic [DVC] pipeline, can be run with `dvc repro`.
* [`apt.txt`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/apt.txt) &mdash; additional `apt-get` dependencies for [Binder].
* [`db/`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/db) &mdash; curated stimulus information, used by [`Dvcfile`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/Dvcfile) to create a [SQLite] database of stimulus information in `db.sqlite`. 
    * [`drifting-gratings.csv`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/db/drifting-gratings.csv)
    * [`schema.sql`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/db/schema.sql)
* [`index.ipynb`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/index.ipynb) &mdash; interactive demonstrations of stimulus, including
    * drifting and counterphase sinusoidal grating animations.
* [`poetry.lock`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/poetry.lock) &mdash; resolved Python dependencies, which can be exported to a `requirements.txt` file with `poetry export -f requirements.txt`.
* [`pyproject.toml`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/pyproject.toml) &mdash; [Poetry] project file, which specifies Python dependencies and project meta-data. 
* [`requirements.txt`](https://gitlab.com/lcg/v2/dataset/python-stimuli/blob/master/requirements.txt) &mdash; `requiremens.txt` file (required by [Binder]) automatically exported prior to `git commit` due to this repository's [pre-commit] configuration.

[Unreleased]: https://gitlab.com/lcg/v2/dataset/python-stimuli/compare?from=release&to=master
[0.1.0]: https://gitlab.com/lcg/v2/dataset/python-stimuli/tags/0.1.0

[Binder]: https://mybinder.org
[DVC]: https://dvc.org
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[MIT License]: https://opensource.org/licenses/MIT
[Poetry]: http://poetry.eustace.io/
[SQLite]: https://www.sqlite.org/index.html
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[bumpversion]: https://pypi.org/project/bumpversion/
[pre-commit]: https://pre-commit.com/

